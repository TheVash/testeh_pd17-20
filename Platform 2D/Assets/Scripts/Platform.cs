﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MovementType
{
    None,
    Horizontal,
    Vertical
}
public class Platform : MonoBehaviour
{
    public MovementType myMovementType;
    public Color gizmoColor = Color.red;
    public float speed;
    public float max;
    public float min;

    private bool movingRight = true;


    void Start()
    {

    }


    void Update()
    {
        if (myMovementType == MovementType.None)
            return;

        switch (myMovementType)
        {
            case MovementType.Horizontal:
                if (movingRight)
                {
                    transform.Translate(Vector2.right * speed * Time.deltaTime, Space.World);
                    if (transform.position.x > max)
                        movingRight = false;
                }
                else
                {
                    transform.Translate(-Vector2.right * speed * Time.deltaTime, Space.World);
                    if (transform.position.x < min)
                        movingRight = true;
                }
                break;
            case MovementType.Vertical:
                if (movingRight)
                {
                    transform.Translate(Vector2.up * speed * Time.deltaTime, Space.World);
                    if (transform.position.y > max)
                        movingRight = false;
                }
                else
                {
                    transform.Translate(-Vector2.up * speed * Time.deltaTime, Space.World);
                    if (transform.position.y < min)
                        movingRight = true;
                }
                break;
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        collision.transform.SetParent(this.transform);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        collision.transform.SetParent(null);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = gizmoColor;
        switch (myMovementType)
        {
            case MovementType.Horizontal:
                Gizmos.DrawLine(
                    new Vector3(min, transform.position.y, 0),
                    new Vector3(max, transform.position.y, 0));
                break;
            case MovementType.Vertical:
                Gizmos.DrawLine(
                    new Vector3(transform.position.x, min, 0), 
                    new Vector3(transform.position.x, max, 0));
                break;
        }
    }
}
