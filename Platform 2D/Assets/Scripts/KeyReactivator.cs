﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyReactivator : MonoBehaviour
{
    private GameObject[] myKeys;

	
	void Start ()
    {
        myKeys = new GameObject[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            myKeys[i] = transform.GetChild(i).gameObject;
        }
	}
	
	public void ReactivateAllKeys()
    {
        foreach (var key in myKeys)
        {
            key.SetActive(true);
        }
    }
}
