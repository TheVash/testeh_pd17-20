﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum AudioNames
{
    Jump,
    ItemPickup
}
public class PlayerController : MonoBehaviour
{
    #region Parametri e variabili

    [Header("Setup")]
    [Range(0.1f, 5)]
    [Tooltip("la velocità del giocatore")]
    public float moveSpeed;
    [Range(1, 15)]
    public float jumpForce;
    [Header("Audio Clips")]
    [Tooltip("Used to see enum order")]
    public AudioNames enumshowcases;
    public AudioClip[] audioClips;
    [Header("Gizmo Setup")]
    public float groundCheckDistance = 0.1f;
    public float gizmoJumpRange;

    //local variables
    [HideInInspector]
    public Door actualDoor;
    private AudioSource speaker;
    private Text keycount_txt;
    private int keyCount = 0;
    private Rigidbody2D rBody;
    private Animator anim;
    #endregion

    #region Ciclo Monobehaviour
    void Awake()
    {
        keycount_txt = GameObject.FindGameObjectWithTag("UIKeyCount").GetComponent<Text>();
        rBody = GetComponent<Rigidbody2D>();
        anim = transform.GetComponentInChildren<Animator>();
        speaker = GetComponent<AudioSource>();

        UpdateKeyCount();
        DontDestroyOnLoad(this.gameObject);
    }

    void FixedUpdate()
    {
        Jump();
    }

    void Update()
    {
        Move();
        CheckDoorInteraction();
    }
    #endregion

    #region Movimenti Player
    /// <summary>
    /// Metodo chiamato in Update() per muovere il player
    /// </summary>
    private void Move()
    {
        float movement = Input.GetAxis("Horizontal");

        if (movement < 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else if (movement > 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }

        if (movement == 0)
            anim.SetBool("IsWalking", false);
        else
            anim.SetBool("IsWalking", true);

        Vector3 dir = new Vector3(movement * moveSpeed * Time.deltaTime, 0, 0);
        transform.Translate(dir);
    }

    private void Jump()
    {
        if (Input.GetButtonDown("Jump") && GroundCheck())
        {
            rBody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            PlayClip(AudioNames.Jump);
        }
    }



    private bool GroundCheck()
    {
        bool result = false;

        Debug.DrawRay(transform.position, Vector3.down * groundCheckDistance, Color.red, 1f);

        if (Physics2D.Raycast(transform.position, Vector2.down, groundCheckDistance))
            result = true;
        return result;
    }
    #endregion

    #region Interazioni
    private void CheckDoorInteraction()
    {
        if (Input.GetButtonDown("OpenDoor"))
        {
            if (actualDoor != null)
            {
                if (UseKeys(actualDoor.GetNeededKeys()))
                {
                    //ho abbastanza chiavi e le ho già consumate
                    actualDoor.OperateDoor(true);
                }
                else
                {
                    //non ho abbastanza chiavi
                    actualDoor.OperateDoor(false);
                }
            }
        }
    }

    public void AddKey(int numberToAdd = 1)
    {
        if (numberToAdd != 1)
            keyCount += numberToAdd;
        else
            keyCount++;

        PlayClip(AudioNames.ItemPickup);
        UpdateKeyCount();
    }

    public bool UseKeys(int numberToUse)
    {
        if (keyCount >= numberToUse)
        {
            keyCount -= numberToUse;
            UpdateKeyCount();
            return true;
        }
        else
            return false;
    }

    public void TakeDamage()
    {
        //FindObjectOfType<GameManager>().ChangeState(GameState.GameOver);
        GameManager.ChangeState(GameState.GameOver);
    }

    #endregion

    #region Metodi UI
    public void UpdateKeyCount()
    {
        keycount_txt.text = keyCount.ToString();
    }
    #endregion

    #region Audio Management
    private void PlayClip(AudioNames clipToLaunch)
    {
        speaker.clip = audioClips[(int)clipToLaunch];
        speaker.Play();
    }

    #endregion

    #region Sistema
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, gizmoJumpRange);
    }
    #endregion
}
