﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    public int keyValue = 1;




    private void OnCollisionEnter2D(Collision2D collision)
    {
        PlayerController player = collision.collider.gameObject.GetComponent<PlayerController>();

        if (player != null)
        {
            player.AddKey(keyValue);
            gameObject.SetActive(false);
        }
    }
}
