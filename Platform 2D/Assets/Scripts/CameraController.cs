﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [Header("Items")]
    public GameObject player;
    public Vector3 offset;
    [Header("Smoothing")]
    [Range(0,1)]
    public float lerpAmount = 0.1f;


    void Start()
    {

    }
    
    void LateUpdate()
    {
        Vector3 newPosition = player.transform.position + offset;
        
        transform.position = Vector3.Lerp(transform.position, newPosition, lerpAmount);
    }
}
