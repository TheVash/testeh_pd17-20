﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    MainMenu,
    PlayMode,
    Paused,
    GameOver,
    GameWin
}
public class GameManager : MonoBehaviour
{
    public GameState actualState = GameState.MainMenu;
    public GameObject[] Uis;

    public static int sceneID;

    public static GameManager instance;

    //local variables
    private GameState previousState = GameState.MainMenu;
    private Vector3 playerStartPosition;


    void Start()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        ChangeState(GameState.MainMenu);
    }


    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if (actualState == GameState.PlayMode) ChangeState(GameState.Paused);
            else if (actualState == GameState.Paused) ChangeState(GameState.PlayMode);
        }
    }

    public static void ChangeState(GameState newState)
    {
        instance.previousState = instance.actualState;
        instance.actualState = newState;

        switch (newState)
        {
            case GameState.MainMenu:
                #region main menu
                Time.timeScale = 0;
                instance.playerStartPosition = FindObjectOfType<PlayerController>().gameObject.transform.position;
                instance.ChangeUI(GameState.MainMenu);
                #endregion
                break;
            case GameState.PlayMode:
                #region play mode
                //attivare ui
                instance.ChangeUI(GameState.PlayMode);

                //player movable
                //timescale
                Time.timeScale = 1;

                //if (previousState == GameState.Paused)
                //    return;
                //if (previousState == GameState.PlayMode)
                //    return;

                switch (instance.previousState)
                {
                    case GameState.MainMenu:
                    case GameState.GameOver:
                    case GameState.GameWin:
                        //resettare posizione player
                        FindObjectOfType<PlayerController>().gameObject.transform.position = instance.playerStartPosition;
                        //keys
                        FindObjectOfType<KeyReactivator>().ReactivateAllKeys();
                        //doors
                        Door[] doors = FindObjectsOfType<Door>();
                        foreach (var door in doors)
                        {
                            door.CloseDoor();
                        }
                        break;
                }

                //if (previousState != GameState.Paused && previousState != GameState.PlayMode)
                //{
                //    //resettare posizione player
                //    FindObjectOfType<PlayerController>().gameObject.transform.position = playerStartPosition;
                //    //keys
                //    FindObjectOfType<KeyReactivator>().ReactivateAllKeys();
                //    //doors
                //    Door[] doors = FindObjectsOfType<Door>();
                //    foreach (var door in doors)
                //    {
                //        door.CloseDoor();
                //    }
                //}
                #endregion
                break;
            case GameState.Paused:
                Time.timeScale = 0;
                instance.ChangeUI(GameState.Paused);
                break;
            case GameState.GameOver:
                instance.StartCoroutine(instance.TimedGameOver());
                break;
            case GameState.GameWin:

                break;
        }

    }

    #region Ui Events
    public void PlayGame()
    {
        ChangeState(GameState.PlayMode);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void BackToMain()
    {
        ChangeState(GameState.MainMenu);
    }
    #endregion

    #region Controls
    public void ChangeUI(GameState id)
    {
        for (int i = 0; i < Uis.Length; i++)
        {
            if (i == (int)id)
                Uis[i].SetActive(true);
            else
                Uis[i].SetActive(false);
        }



        //foreach (var ui in Uis)
        //{
        //    ui.SetActive(false);
        //}
        //
        //Uis[id].SetActive(true);

    }
    #endregion

    IEnumerator TimedGameOver()
    {
        yield return new WaitForSeconds(2);
        instance.ChangeUI(GameState.GameOver);
    }
}
