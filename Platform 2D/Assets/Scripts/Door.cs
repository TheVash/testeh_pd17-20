﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum UiStatus
{
    Disabled,
    Button,
    Keys
}
public class Door : MonoBehaviour
{
    [SerializeField]
    private int neededKeys = 1;
    public AudioClip openDoor;
    public Collider2D doorCollider;
    public GameObject button_UI;
    public GameObject keys_UI;

    private AudioSource speaker;
    private PlayerController player;
    private bool isOpen = false;

    void Start()
    {
        OperateUI(UiStatus.Disabled);
        player = FindObjectOfType<PlayerController>();
        speaker = GetComponent<AudioSource>();
    }

    private void OperateUI(UiStatus statusToGo)
    {
        switch (statusToGo)
        {
            case UiStatus.Disabled:
                button_UI.SetActive(false);
                keys_UI.SetActive(false);
                break;
            case UiStatus.Button:
                button_UI.SetActive(true);
                keys_UI.SetActive(false);
                break;
            case UiStatus.Keys:
                button_UI.SetActive(false);
                keys_UI.SetActive(true);
                break;
        }
    }

    private void OpenDoor()
    {
        doorCollider.enabled = false;
        OperateUI(UiStatus.Disabled);
        speaker.PlayOneShot(openDoor);
        isOpen = true;
    }

    public void CloseDoor()
    {
        doorCollider.enabled = true;
        OperateUI(UiStatus.Disabled);
        isOpen = false;
    }

    public void OperateDoor(bool result)
    {
        if(result == true)
            OpenDoor();
        else
            OperateUI(UiStatus.Keys);
    }

    public int GetNeededKeys()
    {
        return neededKeys;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isOpen)
            return;

        if (collision.gameObject.tag == "Player")
        {
            OperateUI(UiStatus.Button);
            player.actualDoor = this;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            OperateUI(UiStatus.Disabled);
            player.actualDoor = null;
        }
    }
}
